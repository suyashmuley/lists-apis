import { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
import { url } from "inspector";
dotenv.config();

const API_KEY: string | undefined = process.env.API_KEY;
const baseUri: string | undefined = process.env.BASE_URL;

// Home Page
const homePage = async (req: Request, res: Response) => {
  res.send("Hello");
};

// GET API for all Lists
const getAllLists = async (req: Request, res: Response) => {
  const getAllListsUri: string = `${baseUri}?hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getAllListsUri);
    const data: any = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

// GET API for create list page
const createListGet = async (req: Request, res: Response) => {
  const getListUri = `${baseUri}?hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getListUri);
    const data = resp.data;
    res.render("lists");
  } catch (error) {
    console.log(error);
  }
};

// POST API for creating a Marketing List
const createList = async (req: Request, res: Response) => {
  const update = {
    name: "Marketing",
    dynamic: true,
    portalId: 62515,
    filters: [
      [
        {
          operator: "IS_NOT_EMPTY",
          // value: "Marketing contact",
          property: "hs_marketable_status",
          type: "string",
        },
      ],
    ],
  };
  const createListUri = `${baseUri}?hapikey=${API_KEY}`;
  const headers = {
    "Content-Type": "application/json",
  };
  try {
    await axios.post(createListUri, update, { headers });
  } catch (error) {
    console.log(error);
  }
};

const getListById = async (req: Request, res: Response) => {
  const getListByIdUri: string = `${baseUri}/4?hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getListByIdUri);
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

//GET API for a group of contact lists
const getGroupLists = async (req: Request, res: Response) => {
  const getGroupListsUri: string = `${baseUri}/batch?listId=1&listId=4&hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getGroupListsUri);
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

//GET API for Dynamic Lists
const getDynamicLists = async (req: Request, res: Response) => {
  const getDynamicListsUri: string = `${baseUri}/dynamic?count=2&hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getDynamicListsUri);
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

//GET API for Recently added contacts
const getRecentContacts = async (req: Request, res: Response) => {
  const getRecentContactsUri: string = `${baseUri}/4/contacts/recent?hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getRecentContactsUri);
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

//GET API for render contact list page
const getContactLists = async (req: Request, res: Response) => {
  const getContactListsUri = `${baseUri}?hapikey=${API_KEY}`;
  try {
    const resp = await axios.get(getContactListsUri);
    // const data = resp.data;
    res.render("add");
  } catch (error) {
    console.log(error);
  }
};

// Post API to add Contact to Static Lists
const addContactList = async (req: Request, res: Response) => {
  const update = {
    vids: [251],
    emails: ["jondoe@email.com"],
  };
  const addContactListUri = `${baseUri}/12/add?hapikey=${API_KEY}`;
  const headers = {
    "Content-Type": "application/json",
  };
  try {
    await axios.post(addContactListUri, update, { headers });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};

export default {
  homePage,
  getAllLists,
  createList,
  createListGet,
  getListById,
  getGroupLists,
  getDynamicLists,
  getRecentContacts,
  addContactList,
  getContactLists,
};
