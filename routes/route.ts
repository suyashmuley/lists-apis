import { Router } from "express";
import controller from "../controllers/controller";
const router = Router();

router.get("/", controller.homePage);

router.get("/contact-lists", controller.getAllLists);

router.get("/create-list", controller.createListGet);

router.post("/create-list", controller.createList);

router.get("/book-list", controller.getListById);

router.get("/group-lists", controller.getGroupLists);

router.get("/dynamic", controller.getDynamicLists);

router.get("/recent", controller.getRecentContacts);

router.get("/add", controller.getContactLists);

router.post("/add", controller.addContactList);

module.exports = router;
